<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%transaction_service}}`.
 */
class m210302_064639_create_transaction_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%transaction_service}}', [
            'id' => $this->primaryKey(),
            'token' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%transaction_service}}');
    }
}
