<?php

use yii\db\Migration;

/**
 * Class m210228_074209_create_table_user_wallet
 */
class m210228_074209_create_table_user_wallet extends Migration
{
    const TABLE_NAME = 'user_wallet';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id' => $this->primaryKey()->notNull(),
                'user_id' => $this->integer()->notNull(),
                'sum' => $this->decimal(15, 4)->notNull()
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
