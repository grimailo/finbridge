<?php

use yii\db\Migration;

/**
 * Class m210228_074148_create_table_transaction
 */
class m210228_074148_create_table_transaction extends Migration
{
    const TABLE_NAME = 'transaction';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(
            self::TABLE_NAME,
            [
                'id' => $this->primaryKey()->notNull(),
                'sum' => $this->decimal(15, 2)->notNull(),
                'commission' => $this->decimal(5, 2)->notNull(),
                'order_number' => $this->integer()->notNull()
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }

}
