<?php

use yii\db\Migration;

/**
 * Class m210301_154914_alter_column_user_wallet_table
 */
class m210301_154914_alter_column_user_wallet_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('user_wallet', 'sum', $this->decimal(15, 2)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210301_154914_alter_column_user_wallet_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210301_154914_alter_column_user_wallet_table cannot be reverted.\n";

        return false;
    }
    */
}
