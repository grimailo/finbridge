<?php

namespace app\modules\acceptance_payment\controllers;

use app\modules\acceptance_payment\models\DigitalSignature;
use app\modules\acceptance_payment\models\PaymentJob;
use app\modules\acceptance_payment\models\TransactionService;
use app\modules\acceptance_payment\models\UserWallet;
use \yii\rest\Controller;

class AcceptanceController extends Controller
{
    public function actionIndex()
    {

        $data = json_decode(\Yii::$app->request->post()[0], true);
        if (isset($data['signature']) && isset($data['token'])) {
            $signature = $data['signature'];
            $token = $data['token'];
            unset($data['signature']);
            $transactionService = TransactionService::find()->where(['token' => $token])->one();
            $digitalSignature = new DigitalSignature($transactionService->id);

            if ($digitalSignature->verifySignature($data, $signature)) {
                unset($data['token']);
                for ($i = 0; $i < count($data); $i++) {
                    $sum = UserWallet::sumWithCommission($data[$i]['sum'], $data[$i]['commission']);
                    \Yii::$app->queue->push(new PaymentJob([
                        'id' => $data[$i]['id'],
                        'user_id' => $data[$i]['order_number'],
                        'sum' => $sum
                    ]));
                }
            }
        }
        return true;
    }


    public function actionRegistration()
    {
        $transactionService = new TransactionService();
        $transactionService->save();
        $transactionService->makeToken();
        $transactionService->save();
        return $transactionService->token;
    }

    public function actionKey()
    {
        $token = \Yii::$app->request->get()[0];
        $transactionService = TransactionService::find()->where(['token' => $token])->one();
        if ($transactionService) {
            $keyGenerator = new DigitalSignature($transactionService->id);
            return $keyGenerator->generateKeys();
        }
    }

}