<?php

namespace app\modules\acceptance_payment\models;

use Yii;

/**
 * This is the model class for table "transaction_service".
 *
 * @property int $id
 * @property string|null $token
 */
class TransactionService extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['token'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'token' => 'Token',
        ];
    }

    public function makeToken()
    {
        $this->token =  md5($this->id);
    }
}
