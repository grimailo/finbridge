<?php

namespace app\modules\acceptance_payment\models;


class DigitalSignature
{
    /**
     * @var string
     */
    private $keyPath = __DIR__ . '/../src/files/';

    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var string
     */
    private $publicKey;

    /**
     * @var int
     */
    private $idService;

    public function __construct($idService)
    {
        $this->idService = $idService;
    }

    public function generateKeys()
    {
        $path = $this->findPathToKey();
        if (!file_exists($path || !file_get_contents($path))) {
            $new_key_pair = openssl_pkey_new();
            openssl_pkey_export($new_key_pair, $this->privateKey);

            $details = openssl_pkey_get_details($new_key_pair);
            $this->publicKey = $details['key'];
            file_put_contents($path, $this->publicKey);
            return $this->privateKey;
        }
    }

    /**
     * @param $data
     * @param $signature
     * @return int
     */
    public function verifySignature($data, $signature)
    {
        $sign = base64_decode($signature);
        $publicKey = $this->getPubliKey();
        $jsonData = json_encode($data, JSON_FORCE_OBJECT);
        return openssl_verify($jsonData, $sign, $publicKey);
    }

    /**
     * @return false|string
     */
    private function getPubliKey()
    {
        $pathKey = $this->findPathToKey();
        return file_get_contents($pathKey);
    }

    /**
     * @return string
     */
    private function findPathToKey()
    {
        return $this->keyPath . $this->idService . '.pem';
    }
}