<?php

namespace app\modules\acceptance_payment\models;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class PaymentJob extends BaseObject implements JobInterface
{
    public $id;
    public $user_id;
    public $sum;

    /**
     * @param \yii\queue\Queue $queue
     * @return mixed|void
     */
    public function execute($queue)
    {
        $userWallet = new UserWallet();
        $userWallet->id = $this->id;
        $userWallet->user_id = $this->user_id;
        $userWallet->sum = $this->sum;
        $userWallet->save();
    }
}