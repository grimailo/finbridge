<?php

namespace app\modules\system_payment\interfaces;

interface ServerRequest
{
    public function request($address, $method, $data = null);
}