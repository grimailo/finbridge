<?php

namespace app\modules\system_payment\interfaces;

interface GetToken
{
    public function getToken();
}