<?php

namespace app\modules\system_payment\models;

class TransactionCreator
{
    /**
     * @var array
     */
    private $rangeValues = [
        'sum' =>
            [
                'min' => 10,
                'max' => 500
            ],
        'commission' =>
            [
                'min' => 5,
                'max' => 20,
                'divisor' => 10
            ],
        'order_number' =>
            [
                'min' => 1,
                'max' => 20
            ]

    ];

    /**
     * @return Transaction
     */
    public function generateTransaction()
    {
        $transaction = $this->getTransaction();
        $values = $this->generateValues();
        $transaction->load($values, '');
        $transaction->save();
        return $transaction;
    }

    /**
     * @return array
     */
    private function generateValues()
    {
        $arr = [];
        foreach ($this->rangeValues as $key => $value) {
            $arr[$key] = $this->random($this->rangeValues[$key]['min'], $this->rangeValues[$key]['max'], $key);
        }
        return $arr;
    }

    /**
     * @param $min
     * @param $max
     * @param $property
     * @return float|int
     */
    private function random($min, $max, $property)
    {
        $result = rand($min, $max);
        if ($property == 'commission') {
            $result = $result / $this->rangeValues[$property]['divisor'];
        }
        return $result;
    }

    /**
     * @return Transaction
     */
    private function getTransaction()
    {
        return new Transaction();
    }
}