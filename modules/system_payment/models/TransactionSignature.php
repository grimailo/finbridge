<?php

namespace app\modules\system_payment\models;

use app\modules\system_payment\interfaces\GetToken;
use app\modules\system_payment\interfaces\ServerRequest;
use yii\console\Exception;

class TransactionSignature
{
    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var string
     */
    private $privateKeyPath = __DIR__ . '/../src/files/private_key.pem';

    private $request;

    /**
     * TransactionSignature constructor.
     */
    public function __construct(ServerRequest $request, GetToken $get)
    {
        if (!file_exists($this->privateKeyPath) || !file_get_contents($this->privateKeyPath)) {
            $this->request = $request;
            $this->getPrivateKey($get->getToken());
        } else {
            $key = file_get_contents($this->privateKeyPath);
            $this->privateKey = $key;
        }
    }

    /**
     * @param $json
     * @return false|string
     * @throws Exception
     */
    public function sign($json)
    {
        if (!$this->isJson($json)) {
            $data = json_encode($json, JSON_FORCE_OBJECT);
        }

        if (!openssl_sign($data, $this->signature, $this->privateKey)) {
            throw new Exception('openssl_sign failed with error' . openssl_error_string());
        }

        $this->signature = base64_encode($this->signature);
        $data = json_decode($data, true);
        $data['signature'] = $this->signature;
        return json_encode($data);
    }

    /**
     * @param $string
     * @return bool
     */
    private function isJson($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    private function getPrivateKey($token)
    {
        $this->privateKey = $this->request->request(ServiceConnector::PRIVATE_KEY, ServiceConnector::GET, $token);
        file_put_contents($this->privateKeyPath, $this->privateKey);
    }
}