<?php

namespace app\modules\system_payment\models;

use app\modules\system_payment\interfaces\GetToken;
use app\modules\system_payment\interfaces\ServerRequest;

class ServiceToken implements GetToken
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $tokenPath = __DIR__ . '/../src/files/token.json';

    /**
     * @var ServerRequest
     */
    private $request;

    public function __construct(ServerRequest $request)
    {
        if (!file_exists($this->tokenPath) || !file_get_contents($this->tokenPath)) {
            $this->request = $request;
            $this->requestToken();
        } else {
            $token = json_decode(file_get_contents($this->tokenPath));
            $this->token = $token;
        }
    }

    /**
     * @return mixed|string
     */
    public function getToken()
    {
        return $this->token;
    }

    private function requestToken()
    {
        $this->token = $this->request->request(ServiceConnector::REGISTRATION, ServiceConnector::GET);
        file_put_contents($this->tokenPath, json_encode($this->token));
    }

}