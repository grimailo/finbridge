<?php

namespace app\modules\system_payment\models;

use app\modules\system_payment\interfaces\ServerRequest;
use yii\httpclient\Client;
use yii\httpclient\Exception;

class ServiceConnector implements ServerRequest
{
    const SERVER_URL = 'http://finbridge/';
    const TRANSCTION = 'acceptance_payment/acceptance/';
    const REGISTRATION = 'acceptance_payment/acceptance/registration';
    const PRIVATE_KEY = 'acceptance_payment/acceptance/key';
    const POST = 'POST';
    const GET = 'GET';

    public function request($address, $method = 'POST', $data = null)
    {
        try {
            $client = new Client(['baseUrl' => self::SERVER_URL]);
            $response = $client->createRequest()
                ->setMethod($method)
                ->setUrl($address)
                ->setData($data)
                ->send();
            return $response->getData();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}