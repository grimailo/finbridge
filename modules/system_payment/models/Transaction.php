<?php

namespace app\modules\system_payment\models;

use Yii;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property float $sum
 * @property float $commission
 * @property int $order_number
 */
class Transaction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sum', 'commission', 'order_number'], 'required'],
            [['sum', 'commission'], 'number'],
            [['order_number'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sum' => 'Sum',
            'commission' => 'Commission',
            'order_number' => 'Order Number',
        ];
    }
}
