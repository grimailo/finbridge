<?php

namespace app\modules\system_payment\commands;

use app\modules\system_payment\models\ServiceToken;
use app\modules\system_payment\models\ServiceConnector;
use app\modules\system_payment\models\TransactionSignature;
use app\modules\system_payment\models\TransactionCreator;
use \yii\console\Controller;
use yii\console\Exception;
use yii\helpers\Console;

class PayController extends Controller
{

    public function actionIndex()
    {
        $serviceConnector = new ServiceConnector();
        $token = new ServiceToken($serviceConnector);
        $transactionSignature = new TransactionSignature($serviceConnector, $token);
        $marker = true;
        $this->stdout("Запуск эмулятора платежной системы\n", Console::FG_YELLOW);

        while ($marker) {
            $this->stdout("Генерация транзакций\n", Console::FG_CYAN);
            $transactions = [];
            $count = rand(1, 20);

            Console::startProgress(1, $count);
            for ($i = 1; $i <= $count; $i++) {
                $creator = new TransactionCreator();
                $transaction = $creator->generateTransaction();
                $transactions[] = $transaction->attributes;
                Console::updateProgress($i, $count);
            }
            Console::endProgress(PHP_EOL);

            try {
                $transactions['token'] = $token->getToken();
                $signTransactions = $transactionSignature->sign($transactions);
                $this->stdout("Отправка транзакций\n", Console::FG_YELLOW);
                if ($serviceConnector->request(ServiceConnector::TRANSCTION, ServiceConnector::POST,
                    $signTransactions)) {
                    $this->stdout("Успешно отправлено\n", Console::FG_YELLOW);
                }
            } catch (Exception $e) {
                $this->stdout($e->getMessage() . PHP_EOL, Console::FG_RED);
                $marker = false;
            }

            sleep(20);
        }
    }
}